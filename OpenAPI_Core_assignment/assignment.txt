Use the spec.json file and write below 3 testcase-
URL = "https://jsonplaceholder.typicode.com/todos"


1. test_post_request_schema_validation
    TestSteps:
    a. Hit the POST request on URL "https://jsonplaceholder.typicode.com/todos" with valid payload
    b. validate the response against given spec(spec.json)

2. test_get_request_schema_validation
    TestSteps:
    a. Hit the GET request on URL "https://jsonplaceholder.typicode.com/todos"
    b. validate the response against given spec(spec.json)

3. test_get_request_with_userId_queryparameter_schema_validation
    TestSteps:
    a. Hit the GET request on URL "https://jsonplaceholder.typicode.com/todos" with query_parameter = {"userId":2}
    b. validate the response against given spec(spec.json)

Note: 
    1. Use Pytest framework for writing the testcase.
    2. Use fixtures for creating the instance of commonly used classes (like request class)
    3. Use conftest.py file
    4. Only validate the response. No need of request schema validation
    5. Generate a HTML report for test suite
