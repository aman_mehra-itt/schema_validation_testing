from constants import Method
def test_on_get_without_param(spec,object_request_class,object_validator_class):
    url="https://jsonplaceholder.typicode.com/todos"
    get_response_without_param=object_request_class.hit_get_request(url)
    assert get_response_without_param.status_code==200,"Incorrect status code"
    print("Correct status code")
    error=object_validator_class.do_validation(url,get_response_without_param,Method.GET,None,None,spec)
    assert error==0,"Invalid data"
    print("Valid data")

def test_on_get_with_param(spec,object_request_class,object_validator_class):
    url="https://jsonplaceholder.typicode.com/todos"
    parameter={"userId":2}
    get_response_with_param=object_request_class.hit_get_request(url,parameter)
    assert get_response_with_param.status_code==200,"Incorrect status code"
    print("Correct status code")
    error=object_validator_class.do_validation(url,get_response_with_param,Method.GET,parameter,None,spec)
    assert error==0,"Invalid data"
    print("Valid data")

def test_on_post(spec,object_request_class,object_validator_class):
    url="https://jsonplaceholder.typicode.com/todos"
    data=str({'userId': 1, 'id': 1, 'title': 'workout', 'completed': True})
    post_response=object_request_class.hit_post_request(url,data)
    assert post_response.status_code==201,"Incorrect status code"
    print("Correct status code")
    error=object_validator_class.do_validation(url,post_response,Method.POST,None,data,spec)
    assert error==0,"Invalid data"
    print("Valid data")