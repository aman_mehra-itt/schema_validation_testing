import requests
class Request:
    def hit_get_request(self,url,para=None):
        get_response=requests.get(url,params=para)
        return get_response   
    def hit_post_request(self,url,data=None):
        post_response=requests.post(url,data=data)
        return post_response        