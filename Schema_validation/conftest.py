import pytest
from json import load
from openapi_core import create_spec
import hit_request
import validator

def pytest_configure():
    pass

@pytest.fixture()
def spec():
    with open('spec.json', 'r') as spec_file:
        spec_dict = load(spec_file)
    spec = create_spec(spec_dict)
    spec_file.close()
    return spec


@pytest.fixture()
def object_request_class():
    request=hit_request.Request()
    return request

@pytest.fixture()
def object_validator_class():
    request=validator.Validator()
    return request