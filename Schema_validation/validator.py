from openapi_core.validation.response.validators import ResponseValidator
from openapi_core.validation.request.datatypes import OpenAPIRequest
from openapi_core.validation.response.datatypes import OpenAPIResponse
import utility
from constants import ContentType
class Validator:
    def do_validation(self,url,response,method,param,data,spec):
        content_type=utility.Utility()
        mime=content_type.get_mime(response.headers["Content-Type"])
        request_from_api=OpenAPIRequest(url,method,param,data,ContentType.CONTENT_TYPE)
        response_from_api=OpenAPIResponse(response.text,response.status_code,mime)
        validator = ResponseValidator(spec)
        result = validator.validate(request_from_api, response_from_api)
        errors = result.errors
        errors_len=len(errors)
        return errors_len